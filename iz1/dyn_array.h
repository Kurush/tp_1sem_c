#ifndef DIN_ARRAY_H
#define DIN_ARRAY_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MIN_BUF 8 // MIN_BUF must divide MULT_BUF!
#define MULT_BUF 2

#define EPS 1e-9

typedef double arr_type;

typedef struct
{
    arr_type* arr;
    size_t size, max_size;
} array_t;

int exists(array_t *arr)
{
    return (arr && (arr->arr != NULL));
}

void init_arr(array_t *arr)
{
    arr->arr = NULL;
    arr->size = 0;
    arr->max_size = 0;
}
int create_arr(array_t *arr)
{
    if (!arr || exists(arr))
        return EXIT_FAILURE;
    init_arr(arr);
    arr->max_size = MIN_BUF;
    arr->arr = (arr_type*) malloc(arr->max_size * sizeof(arr_type));
    if (!arr->arr)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}
void delete_arr(array_t *arr)
{
    free(arr->arr);
    init_arr(arr);
}

int extend_arr(array_t *arr)
{
    if (!exists(arr))
        return EXIT_FAILURE;
    arr->max_size *= MULT_BUF;
    arr_type* tmp = (arr_type*) realloc(arr->arr, arr->max_size * sizeof(arr_type));
    if (tmp)
    {
        arr->arr = tmp;
        tmp = NULL;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

int input_arr(array_t *arr, FILE *file)
{
    // arr.n must be defined !!!!!!
    if (!exists(arr))
        return EXIT_FAILURE;
    for (size_t i = 0; i < arr->size; ++i)
        if (fscanf(file, "%lf", &arr->arr[i]) != 1)
            return EXIT_FAILURE;
    return EXIT_SUCCESS;
}
int output_arr(array_t *arr, FILE *file)
{
    if (!exists(arr))
        return EXIT_FAILURE;
    for (size_t i = 0; i < arr->size; ++i)
       if (fprintf(file, "%.4lf ", arr->arr[i]) < 0)
           return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

int push_back(array_t *arr, arr_type x)
{
    if (!exists(arr))
        return EXIT_FAILURE;
    if (arr->size == arr->max_size)
    {
        if (extend_arr(arr))
            return EXIT_FAILURE;
    }
    arr->arr[arr->size++] = x;
    return EXIT_SUCCESS;
}

void on_arr_test_exit(FILE *buf, array_t *arr, char *filename)
{
    fclose(buf);
    remove(filename);
    delete_arr(arr);
}


int test_dyn_arr()
{
    // Create

    array_t arr;
    init_arr(&arr);
    if (create_arr(&arr))
        return EXIT_FAILURE;
    if (!create_arr(&arr))
        return EXIT_FAILURE;

    // NULL - testing

    char *filename = "test_dyn_array.txt";
    FILE *buf = fopen(filename, "w");

    if (!input_arr(NULL, buf))
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }
    if (!output_arr(NULL, buf))
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }
    if (!push_back(NULL, 1.2))
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }

    // Filling array, push_back

    double vals[] = {12, -7.5, 0, 144, 1};
    int n = 5;
    for (int i = 0; i < n; ++i)
        if (push_back(&arr, vals[i]))
        {
            on_arr_test_exit(buf, &arr, filename);
            return EXIT_FAILURE;
        }

    // Output / input

    if (output_arr(&arr, buf))
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }
    fclose(buf);

    buf = fopen(filename, "r");
    if (!buf)
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }
    if (input_arr(&arr, buf))
    {
        on_arr_test_exit(buf, &arr, filename);
        return EXIT_FAILURE;
    }
    for (int i = 0; i < n; ++i)
        if (arr.arr[i] != vals[i])
        {
            on_arr_test_exit(buf, &arr, filename);
            return EXIT_FAILURE;
        }
    on_arr_test_exit(buf, &arr, filename);
    return EXIT_SUCCESS;
}


#endif // DIN_ARRAY_H
