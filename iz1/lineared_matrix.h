#ifndef LIN_MATRIX_H
#define LIN_MATRIX_H

#include "dyn_array.h"

typedef struct
{
    array_t arr;
    size_t n, m;
} lin_matrix;

void init_lmatr(lin_matrix *matr)
{
    matr->n = 0;
    matr->m = 0;
    init_arr(&matr->arr);
}

int create_lmatr(lin_matrix *matr, size_t n, size_t m)
{
    if (exists(&matr->arr))
        return EXIT_FAILURE;
    init_arr(&matr->arr);

    matr->n = n;
    matr->m = m;
    if (create_arr(&matr->arr))
        return EXIT_FAILURE;
    for (size_t i = 0; i < n * m; ++i)
        if (push_back(&matr->arr, 0))
        {
            delete_arr(&matr->arr);
            return EXIT_FAILURE;
        };

    return EXIT_SUCCESS;
}
void delete_lmatr(lin_matrix *matr)
{
    delete_arr(&matr->arr);
    init_lmatr(matr);
}

int input_lmatr(lin_matrix *matr, FILE *file)
{
    return input_arr(&matr->arr, file);
}
int output_lmatr(lin_matrix *matr, FILE *file)
{
    if (!exists(&matr->arr))
        return EXIT_FAILURE;
    for (size_t i = 0; i < matr->n; ++i)
    {
        for (size_t j = 0; j < matr->m; ++j)
            if (fprintf(file, "%.4lf ", matr->arr.arr[i * matr->n + j]) < 0)
                return EXIT_FAILURE;
        if (fprintf(file, "\n") < 0)
            return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}



arr_type sum_adjacent(lin_matrix *matr, size_t ind)
{
    arr_type sum = 0;
    if (ind >= matr->n) // up
        sum += matr->arr.arr[ind - matr->n];
    if (ind + matr->n < matr->n * matr->m ) // down
        sum += matr->arr.arr[ind + matr->n];
    if (ind % matr->m >= 1) // left
        sum += matr->arr.arr[ind - 1];
    if ((ind % matr->m) + 1 < matr->m) // right
        sum += matr->arr.arr[ind + 1];

    return sum;
}

int process_linear_matrix (lin_matrix *matr, lin_matrix *res)
{
    if (!exists(&matr->arr))
        return EXIT_FAILURE;
    delete_lmatr(res);
    if (create_lmatr(res, matr->n, matr->m))
        return EXIT_FAILURE;


    for (size_t i = 0; i < matr->n * matr->m; ++i)
        res->arr.arr[i] = sum_adjacent(matr, i);

    return EXIT_SUCCESS;

}

int test_lin_matr()
{
    // Create

    lin_matrix matr, res;
    init_lmatr(&matr);
    init_lmatr(&res);
    if (create_lmatr(&matr, 3, 3))
        return EXIT_FAILURE;
    if (!create_lmatr(&matr, 3, 3))
        return EXIT_FAILURE;

    // NULL - testing

    char *filename = "test_lin_matrix.txt";
    FILE *buf = fopen(filename, "w");

    if (!input_lmatr(NULL, buf))
    {
        fclose(buf);
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }
    if (!output_lmatr(NULL, buf))
    {
        fclose(buf);
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }
    if (!process_linear_matrix(NULL, NULL))
    {
        fclose(buf);
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }

    // Filling matrix

    double vals[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    double res_vals[] = {6, 9, 8, 13, 20, 17, 12, 21, 14};
    int n = 9;
    for (int i = 0; i < n; ++i)
        matr.arr.arr[i] = vals[i];

    // Output / input

    if (output_lmatr(&matr, buf))
    {
        fclose(buf);
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }
    fclose(buf);

    buf = fopen(filename, "r");
    if (!buf)
    {
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }
    if (input_lmatr(&matr, buf))
    {
        fclose(buf);
        remove(filename);
        delete_lmatr(&matr);
        return EXIT_FAILURE;
    }
    fclose(buf);
    remove(filename);

    // Process
    if (process_linear_matrix(&matr, &res))
    {
        delete_lmatr(&matr);
        delete_lmatr(&res);
        return EXIT_FAILURE;
    }

    for (int i = 0; i < n; ++i)
        if (fabs(res.arr.arr[i] - res_vals[i]) > EPS)
        {
            delete_lmatr(&matr);
            delete_lmatr(&res);
            return EXIT_FAILURE;
        }

    delete_lmatr(&matr);
    delete_lmatr(&res);

    return EXIT_SUCCESS;
}

#endif // LIN_MATRIX_H
