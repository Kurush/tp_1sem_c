#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lineared_matrix.h"

int main()
{
    // unit testing
    printf("UNIT TESTING...\n");

    int flag = 1;

    int err = test_dyn_arr();
    printf(">> unit-testing for dyn_array: ");
    if (err)
        puts("FAILURE"), flag = 0;

    else
        puts("OK");

    err = test_lin_matr();
    printf(">> unit-testing for lineared_matrix: ");
    if (err)
        puts("FAILURE"), flag = 0;
    else
        puts("OK");

   if (!flag)
       return EXIT_FAILURE;

    return EXIT_SUCCESS;
}
