#include <stdio.h>
#include "lineared_matrix.h"


/*
Условие ИЗ№1:
Составить программу вычисления новой матрицы на основе исходной таким образом,
чтобы каждый элемент новой матрицы был равен среднему всех смежных с ним элементов исходной.
Вычисления оформить в виде функции, принимающей на вход матрицу,
заданную в виде одномерного линеаризованного массива
(сначала в ней следуют элементы первой строки, затем — второй и т.д.).
На выход функция должна возвращать указатель на результирующую структуру.
*/


int main()
{
    lin_matrix matr, res;
    init_lmatr(&matr);
    init_lmatr(&res);

    long n, m;
    printf("Enter matrix sizes (rows, cols): ");
    if (scanf("%ld %ld", &n, &m) != 2)
    {
        puts ("failed to read");
        return EXIT_FAILURE;
    }

    if (create_lmatr(&matr, n, m))
    {
        puts ("failed to create matrix");
        return EXIT_FAILURE;
    }

    printf("Enter matrix elements (by rows):\n");
    if (input_lmatr(&matr, stdin))
    {
        puts ("failed to read matrix");
        return EXIT_FAILURE;
    }

    if (process_linear_matrix(&matr, &res))
    {
        puts ("failed to process matrix");
        return EXIT_FAILURE;
    }

    if (output_lmatr(&res, stdout))
    {
        puts ("failed to print matrix");
        return EXIT_FAILURE;
    }

    delete_lmatr(&matr);
    delete_lmatr(&res);

    return EXIT_SUCCESS;
}
