#ifndef POINT_PAIR_H
#define POINT_PAIR_H

#include <inttypes.h>
#include <math.h>

#define PPAIR_RETURN_ERROR 1
#define PPAIR_RETURN_SUCCESS 0

typedef struct
{
    char x1, y1, x2, y2;
} point_pair;

int get_coords(int32_t x, point_pair *p)
{
    // split a 4-byte integer into 4 1-byte numbers
    if (!p)
        return PPAIR_RETURN_ERROR;

    short mask = pow(2, 8) - 1;
    p->x1 = x & mask;
    x >>= 8;
    p->y1 = x & mask;
    x >>= 8;
    p->x2 = x & mask;
    x >>= 8;
    p->y2 = x & mask;
    return PPAIR_RETURN_SUCCESS;
}

int64_t get_length(point_pair p)
{
    // calculate distance between points (p.x1, p.y1) and (p.x2, p.y2)
    return (p.x1 - p.x2) * (p.x1 - p.x2) + (p.y1 - p.y2) * (p.y1 - p.y2);
}

int test_point_pair()
{
    point_pair p;
    if (!get_coords(4, NULL))
        return PPAIR_RETURN_ERROR;

    int32_t x = -1;
    if (get_coords(x, &p))
        return PPAIR_RETURN_ERROR;
    if ((p.x1 != -1) || (p.y1 != -1) || (p.x2 != -1) || (p.y2 != -1))
        return PPAIR_RETURN_ERROR;

    point_pair p2;
    x = 1 + 2 * pow(2, 8) + 3 * pow(2, 16) + 4 * pow(2, 24);
    if (get_coords(x, &p2))
        return PPAIR_RETURN_ERROR;
    if ((p2.x1 != 1) || (p2.y1 != 2) || (p2.x2 != 3) || (p2.y2 != 4))
        return PPAIR_RETURN_ERROR;

    return PPAIR_RETURN_SUCCESS;
}

#endif // POINT_PAIR_H
