#include <inttypes.h>
#include <stdlib.h>

#include "point_pair.h"


#define PSUM_RETURN_ERROR 1
#define PSUM_RETURN_SUCCESS 0


int calc_ptr_sum(int32_t *arr, size_t n, int64_t *res)
{
    // calculate sum of distances between points
    if (!arr || !res)
        return PSUM_RETURN_ERROR;
    *res = 0;
    point_pair p;
    for (size_t i = 0; i < n; ++i)
    {
        get_coords(arr[i], &p);
        *res += get_length(p);
    }

    return PSUM_RETURN_SUCCESS;
}
