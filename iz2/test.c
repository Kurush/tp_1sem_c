#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <dlfcn.h>

//#include "point_pair.h"

void *library;
int (*thread_calc_ptr_sum)(int32_t *arr, size_t n, int64_t *res);

int open_lib_load()
{
    // open dynamic library and load a function

    library = dlopen("libmultiflow_ptr_sum.so", RTLD_LAZY);
    if (!library)
        return EXIT_FAILURE;
    thread_calc_ptr_sum = (int (*)(int32_t*, size_t, int64_t*))(void*) dlsym(library, "calc_ptr_sum");
    if (!thread_calc_ptr_sum)
        return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

void close_lib()
{
    dlclose(library);
}



int main()
{
    // unit testing
    printf("UNIT TESTING...\n");

    int flag = 1;

    printf("point pair testing...   ");
    int err = test_point_pair();
    if (err)
    {
        flag = 0;
        puts("FAILURE");
    }
    else
    {
        puts("SUCCESS");
    }


    // func testing
    printf("FUNC TESTING...\n");

    if (open_lib_load())
    {
        puts("failed to open dyn lib");
        return EXIT_FAILURE;
    }

    int64_t a;
    int32_t arr[] = {1,2,3,4,5};
    // negative tests
    if (!calc_ptr_sum(NULL, 1, &a))
    {
        flag = 0;
    }
    if (!calc_ptr_sum(arr, 1, NULL))
    {
        flag = 0;
    }
    if (!thread_calc_ptr_sum(NULL, 1, &a))
    {
        flag = 0;
    }
    if (!thread_calc_ptr_sum(arr, 1, NULL))
    {
        flag = 0;
    }
    if (flag)
        puts("Negative tests: passed");
    else
        puts("Negative tests: errors found");

    // positive tests
    int64_t res1, res2;
    size_t n_vals[] = {1, 100, 10000, 100000000};
    for (int i = 0; i < 4; ++i)
    {
        size_t n = n_vals[i];
        int32_t *arr = malloc(sizeof(int32_t) * n);
        if (!arr)
        {
            close_lib();
            return EXIT_FAILURE;
        }
        for (size_t i = 0; i < n; ++i)
            arr[i] = rand();

        if (calc_ptr_sum(arr, n, &res1))
        {
            flag = 0;
        }
        if (thread_calc_ptr_sum(arr, n, &res2))
        {
            flag = 0;
        }
        if (res1 != res2)
        {
            flag = 0;
            printf("Test %d: error\n", i+1);
        }
        else
        {
            printf("Test %d: OK\n", i+1);
        }
        free(arr);
    }

    close_lib();



   if (!flag)
   {
       puts("TESTING FAILED");
       return EXIT_FAILURE;
   }
   else
   {
       puts("TESTING SUCCESSFUL");
   }

    return EXIT_SUCCESS;
}
