/* command line arguments:
 app.exe n t file
 - n - integer, size of array
 - t - type of action, one of {0, 1, 2}:
   * 0 - run on static lib
   * 1 - run on dynamic lib
   * 2 - run both & compare efficiency
 - file - file name, where to output results
*/

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timeb.h>

#include <pthread.h>

#include <dlfcn.h>

/*
ИЗ2 посвящено приобретению навыков системной разработки на C и работе с внешними библиотеками.
В качестве результата ИЗ2 ожидается:
* грамотное разбиение проекта на файлы;
* наличие двух реализаций – последовательной и параллельной, оформленных в виде статической
  и динамической библиотеки, а также тестирующей программы, которая будет сравнивать на разных
  входных данных результаты обеих реализаций между собой;
* максимальная утилизация ресурсов процессора при параллельной обработке данных
  путём использования нескольких процессов или потоков;
* продуманные структуры данных в целях экономии оперативной памяти;
* реализация алгоритмов, эффективно взаимодействующих с кэш-памятью.

Сравните и выведите в консоль время работы последовательного и параллельного
с использованием нескольких потоков алгоритмов, каждый из которых выделяет в динамической памяти
массив 4-байтовых чисел размером 100 Мб и, рассматривая каждое значение типа _int32_t как кортеж координат
(x1, y1, x2, y2), где каждая координата может принимать значение от -128 до 127,
последовательно вычисляет длину пути от первой до последней точки на координатной плоскости.
*/



void *library;
int (*thread_calc_ptr_sum)(int32_t *arr, size_t n, int64_t *res);

int open_lib_load()
{
    // open dynamic library and load a function

    library = dlopen("libmultiflow_ptr_sum.so", RTLD_LAZY);
    if (!library)
        return EXIT_FAILURE;
    thread_calc_ptr_sum = (int (*)(int32_t*, size_t, int64_t*))(void*) dlsym(library, "calc_ptr_sum");
    if (!thread_calc_ptr_sum)
        return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

void close_lib()
{
    dlclose(library);
}


int main(int argc, char** argv)
{
    if (argc != 4)
        return EXIT_FAILURE;
    size_t n = (size_t) atoi(argv[1]);
    int type = atoi(argv[2]);
    if (!((type == 0) || (type == 1) || (type == 2)))
        return EXIT_FAILURE;
    char* filename = argv[3];

    if (type != 0)
    {
        if (open_lib_load())
        {
            puts("failed to open dyn lib");
            return EXIT_FAILURE;
        }
    }

    // initialization block
    struct timeb start, end;

    int64_t res1, res2;
    int32_t *arr = malloc(sizeof(int32_t) * n);
    if (!arr)
    {
        if (type != 0) close_lib();
        return EXIT_FAILURE;
    }
    for (size_t i = 0; i < n; ++i)
        arr[i] = rand();


     // CALCULATIONS
    int64_t time_origin = 0, time_opti = 0;
    int iter_cnt = 10;
    for (int i = 0; i < iter_cnt; ++i)
    {
        if ((type == 0) || (type == 2))  // original calculation (1 process)
        {
            ftime(&start);
            if (calc_ptr_sum(arr, n, &res1))
            {
                if (type != 0) close_lib();
                free(arr);
                return EXIT_FAILURE;
            }
            ftime(&end);
            time_origin += (end.time - start.time + 0.) + (end.millitm - start.millitm + 0.) / 1000;
        }

        if ((type == 1) || (type == 2))  // advanced calculation (several processes)
        {
            ftime(&start);
            if (thread_calc_ptr_sum(arr, n, &res2))
            {
                if (type != 0) close_lib();
                free(arr);
                return EXIT_FAILURE;
            }
            ftime(&end);
            time_opti += (end.time - start.time + 0.) + (end.millitm - start.millitm + 0.) / 1000;
        }
    }

    free(arr);
    if (type != 0) close_lib();


    // OUTPUT results
    FILE *file = fopen(filename, "w");
    if (!file)
    {
        return EXIT_FAILURE;
    }

    if (fprintf(file, "Summing length between %ld points...\n", (long) n) < 0)
    {
        fclose(file);
        return EXIT_FAILURE;
    }
    if (fprintf(file, "Time info averaged on %d runs:\n========\n", iter_cnt) < 0)
    {
        fclose(file);
        return EXIT_FAILURE;
    }
    if ((type != 1) &&
        (fprintf(file, "ORIGINAL ALGORITHM\n sum: %ld\n time needed: %ld ms\n",
                 (long)res1, (long)time_origin) < 0))
    {
        fclose(file);
        return EXIT_FAILURE;
    }
    if ((type != 0) &&
        (fprintf(file, "MULTI-PROCESS ALGORITHM\n sum: %ld\n time needed: %ld ms\n",
                 (long)res2, (long)time_opti) < 0))
    {
        fclose(file);
        return EXIT_FAILURE;
    }
    fclose(file);
    puts("OK");
    return EXIT_SUCCESS;
}
