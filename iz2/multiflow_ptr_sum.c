#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>

#include "point_pair.h"

#define MIN_SUBARR_SIZE 10

#define MFLOW_RETURN_ERROR 1
#define MFLOW_RETURN_SUCCESS 0

typedef struct
{
    size_t len;
    int64_t sum;
    int32_t *arr;
} thread_args;


void* run_ptr_sum_partial(void *void_args)
{
    thread_args *args = (thread_args*) void_args;

    args->sum = 0;
    point_pair p;
    for (size_t i = 0; i < args->len; ++i)
    {
        get_coords(args->arr[i], &p);
        args->sum  += get_length(p);
    }
    pthread_exit(0);
}

int calc_ptr_sum(int32_t *arr, size_t n, int64_t *res)
{
    // calculate manually, array size's too little to bother with threads
    if (!arr || !res)
        return MFLOW_RETURN_ERROR;

    int numCPU = sysconf(_SC_NPROCESSORS_ONLN);
    int threads_cnt = numCPU;

    if (n / threads_cnt < MIN_SUBARR_SIZE)
    {
        *res = 0;
        point_pair p;
        for (size_t i = 0; i < n; ++i)
        {
            get_coords(arr[i], &p);
            *res += get_length(p);
        }
    }
    else
    {
        // init thread arguments
        thread_args args[threads_cnt];
        size_t len = n / threads_cnt;
        for (int i = 0; i < threads_cnt; ++i)
        {
            args[i].arr = &arr[len * i];
            args[i].len = len;
        }
        args[threads_cnt - 1].len += n % threads_cnt;

        // run threads
        pthread_t threads[threads_cnt];
        for (int i = 0; i < threads_cnt; ++i)
        {
            int errflag;
            errflag = pthread_create(&threads[i], NULL, run_ptr_sum_partial, &args[i]);
            if(errflag != 0)
            {
                return MFLOW_RETURN_ERROR;
            }
        }

        // get threads finished
        for (int i = 0; i < threads_cnt; ++i)
        {
            int errflag;
            errflag = pthread_join(threads[i], NULL);
            if (errflag != 0)
            {
                return MFLOW_RETURN_ERROR;
            }
        }

        // sum results
        *res = 0;
        for (int i = 0; i < threads_cnt; ++i)
            *res += args[i].sum;
    }

    return MFLOW_RETURN_SUCCESS;
}
